let form = document.getElementById("password-form");
let wrapper = document.querySelectorAll(".input-wrapper");

wrapper.forEach(element => {
  element.addEventListener("mousedown", (e) => {
    if (
      (e.target.classList.contains("icon-password") == true) &&
      (element.childNodes[1].type === "password")
    ) {
      element.childNodes[1].type = "text";
      e.target.classList.remove("fa-eye");
      e.target.classList.add("fa-eye-slash");
    }
  });
  element.addEventListener("mouseup", (e) => {
    if (
      e.target.classList.contains("icon-password") == true &&
      element.childNodes[1].type === "text"
    ) {
      element.childNodes[1].type = "password";
      e.target.classList.remove("fa-eye-slash");
      e.target.classList.add("fa-eye");
    }
  });
});

function validate() {
  let passwordsNodes = Array.from(
    document.querySelectorAll(".password-form input[type=password]"));
  let passwords = passwordsNodes.map((item) => item.value);
  let errorMessage = document.getElementById("message");

  if (passwords[0] != passwords[1]) {
    errorMessage.innerHTML = "Потрібно ввести однакові значення";
    return false;
  }
  else if ((passwords[0].length == 0) || (passwords[1].length == 0)) {
    errorMessage.innerHTML = "Пароль не може бути пустим";
  }
  else {
    errorMessage.innerHTML = "";
    alert("You are welcome");
  }
}

form.addEventListener("submit", () => {
  return validate();
}, false);